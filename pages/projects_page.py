from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from pages.add_project_page import AddProjectsPage

class ProjectsPage:
    add_project_button = (By.CSS_SELECTOR, '.button_link[href*="add_project"]')
    search_input = (By.CSS_SELECTOR, '#search')
    search_button = (By.CSS_SELECTOR, '#j_searchButton')
    project_names = (By.CSS_SELECTOR, 'tbody tr td a')

    def __init__(self, browser):
        self.browser = browser
        self.wait = WebDriverWait(browser, 10)

    def click_add_project_button(self):
        self.wait.until(EC.element_to_be_clickable(self.add_project_button)).click()
        self.wait.until(EC.url_contains("add_project"))
        return AddProjectsPage(self.browser)

    def search_for_project(self, project_name):
        self.wait.until(EC.element_to_be_clickable(self.search_input)).send_keys(project_name)
        self.wait.until(EC.element_to_be_clickable(self.search_button)).click()
        return self

    def verify_project_is_added(self, project_name):
        try:
            self.wait.until(lambda x: any(project_name in element.text for element in
                                          self.browser.find_elements(*self.project_names)))
            return True

        except TimeoutException:
            print(f"Error: The expected text '{project_name}' was not found in any project_name element within the given time.")
            return False



