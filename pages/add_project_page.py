from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class AddProjectsPage:
    name_input = (By.CSS_SELECTOR, '#name')
    prefix_input = (By.CSS_SELECTOR, '#prefix')
    save_button = (By.CSS_SELECTOR, '#save')
    success_message = (By.CSS_SELECTOR, '#j_info_box p')

    def __init__(self, browser):
        self.browser = browser
        self.wait = WebDriverWait(browser, 10)

    def add_new_project(self, project_name, prefix):
        self.wait.until(EC.presence_of_element_located(self.name_input)).send_keys(project_name)
        self.wait.until(EC.presence_of_element_located(self.prefix_input)).send_keys(prefix)
        self.wait.until(EC.element_to_be_clickable(self.save_button)).click()

    def get_success_message(self):
        message_box = self.wait.until(EC.presence_of_element_located(self.success_message))
        self.wait.until(EC.visibility_of(message_box))
        return message_box.text

