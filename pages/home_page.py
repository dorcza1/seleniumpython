from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from pages.messages_page import MessagesPage
from pages.projects_page import ProjectsPage

class HomePage:
    user_email = (By.CSS_SELECTOR, ".user-info small")
    mail_icon = (By.CSS_SELECTOR, '.icon_mail')
    admin_panel_button = (By.CSS_SELECTOR, '.icon_tools.icon-20')
    projects_menu_button = (By.CSS_SELECTOR, '.menu .icon_puzzle_alt')

    def __init__(self, browser):
        self.browser = browser
        self.wait = WebDriverWait(browser, 10)

    def get_current_user_email(self):
        return self.browser.find_element(*self.user_email).text

    def click_mail_icon(self):
        self.browser.find_element(*self.mail_icon).click()
        return MessagesPage(self.browser)

    def click_admin_panel_button(self):
        self.wait.until(EC.element_to_be_clickable(self.admin_panel_button)).click()
        self.wait.until(EC.url_contains("administration"))
        return ProjectsPage(self.browser)

    def click_projects_menu_button(self):
        self.wait.until(EC.element_to_be_clickable(self.projects_menu_button)).click()
        self.wait.until(EC.url_contains("projects"))
        return ProjectsPage(self.browser)


