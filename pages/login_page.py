from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class LoginPage:
    url = 'http://demo.testarena.pl/zaloguj'

    email_input = (By.CSS_SELECTOR, '#email')
    password_input = (By.CSS_SELECTOR, '#password')
    login_button = (By.CSS_SELECTOR, '#login')

    def __init__(self, browser):
        self.browser = browser
        self.wait = WebDriverWait(browser, 10)

    def open_page(self):
        self.browser.get(self.url)
        self.wait.until(EC.presence_of_element_located(self.email_input))
        return self

    def attempt_login(self, email, password):
        self.browser.find_element(*self.email_input).send_keys(email)
        self.browser.find_element(*self.password_input).send_keys(password)
        self.browser.find_element(*self.login_button).click()
