import random
import string
from pages.login_page import LoginPage
from pages.home_page import HomePage
from pages.messages_page import MessagesPage

administrator_email = 'administrator@testarena.pl'

def test_successful_login(chrome_browser):
    login_page = LoginPage(chrome_browser)
    login_page.open_page()
    login_page.attempt_login('administrator@testarena.pl', 'sumXQQ72$L')
    home_page = HomePage(chrome_browser)
    user_email = home_page.get_current_user_email()
    assert administrator_email == user_email

def test_add_message(edge_browser):
    my_text = generate_random_string(10)
    login_page = LoginPage(edge_browser)
    login_page.open_page()
    login_page.attempt_login('administrator@testarena.pl', 'sumXQQ72$L')
    home_page = HomePage(edge_browser)
    home_page.click_mail_icon()
    messages_page = MessagesPage(edge_browser)
    messages_page.add_message(my_text)
    messages_page.verify_message_added(my_text)

def generate_random_string(length=10):
    characters = string.ascii_letters + string.digits
    random_string = ''.join(random.choice(characters) for _ in range(length))
    return random_string