import string
import random
from pages.login_page import LoginPage
from pages.home_page import HomePage
from pages.projects_page import ProjectsPage

project_name = None

def get_random_string(length):
    return ''.join(random.choices(string.ascii_uppercase + string.digits, k=length))

def test_add_new_project_and_verify_its_on_a_list(chrome_browser):
    global project_name
    project_name = get_random_string(10)
    prefix = get_random_string(5)

    login_page = LoginPage(chrome_browser)
    login_page.open_page()
    login_page.attempt_login('administrator@testarena.pl', 'sumXQQ72$L')

    home_page = HomePage(chrome_browser)
    home_page.click_admin_panel_button()

    projects_page = ProjectsPage(chrome_browser)
    add_project_page = projects_page.click_add_project_button()

    add_project_page.add_new_project(project_name, prefix)
    success_message_text = add_project_page.get_success_message()
    assert "Projekt został dodany" in success_message_text

    home_page.click_projects_menu_button()
    projects_page.search_for_project(project_name)
    assert projects_page.verify_project_is_added(project_name)




